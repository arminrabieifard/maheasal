<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%menu_category}}".
 *
 * @property string $id
 * @property string $parent_id زیر شاخه دسته
 * @property string $title عنوان
 * @property int $visible نمایش داده شود؟
 * @property int $create_at زمان افزودن
 *
 * @property Menutalar[] $menutalars
 */
class MenuCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'visible', 'create_at'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'زیر شاخه دسته',
            'title' => 'عنوان',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenutalars()
    {
        return $this->hasMany(Menutalar::className(), ['menu_id' => 'id']);
    }
}
