<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%auth_item}}".
 *
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property User[] $users
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 * @property Role[] $children
 * @property Role[] $parents
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, /*'targetClass' => AuthRule::className(),*/ 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%auth_assignment}}', ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Role::className(), ['name' => 'child'])->viaTable('{{%auth_item_child}}', ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Role::className(), ['name' => 'parent'])->viaTable('{{%auth_item_child}}', ['child' => 'name']);
    }

    public function all_rols()
    {
        return[
            'news' => [
                ['name' => 'admin_post','label' => 'Add post','checked' => 0 ],
                ['name' => 'add_post','label' => 'Add post','checked' => 0 ],
                ['name' => 'edit_post','label' => 'Edit post','checked' => 0 ],
                ['name' => 'delete_post','label' => 'Delete post','checked' => 0 ],
            ]
        ];
    }

    public function getAllRoles()
    {
        $roles = $this->all_rols();
        if(!$this->isNewRecord){
            $db_all_roles = (new\yii\db\query())
            ->select(['child'])
            ->from(['auth_item_child'])
            ->where(['parent' => $this->name])
            ->all();
            $db_roles =[];
            foreach ($db_all_roles as $k => $v){
                array_push($db_roles,$v['child']);
            }
             foreach ($roles as $kr => $vr){
                foreach ($vr as $ki => $item){
                    if(in_array($item['name'],$db_roles )){
                        $roles[$kr][$ki]['checked'] = 1;
                    }
                }

             }
         }
        return $roles;

    }

    public function save($runValidation = true, $attributeNames = NULL)
    {
        $auth = Yii::$app->authManager;
        $item = Yii::$app->request->post("Items");
        $time = time();
        $sql = "DELETE FROM `auth_item_child` WHERE `parent` = '{$this->name}'";
        Yii::$app->db->createCommand($sql)->query();
        $sql = "INSERT IGNORE INTO `auth_item`(`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES('{$this->name}',1,'{$this->description}',NULL ,NULL ,$time,$time) ";
        Yii::$app->db->createCommand($sql)->query();
         foreach ($item as $k => $v){
             $sql = "INSERT IGNORE INTO `auth_item`(`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES('{$k}',2,'{$k}',NULL ,NULL ,$time,$time) ";
            Yii::$app->db->createCommand($sql)->query();
            $sql = "INSERT IGNORE INTO `auth_item_child`(`parent`, `child`) VALUES ('{$this->name}','{$k}')";
             Yii::$app->db->createCommand($sql)->query();
         }

        return true;
        /* // add "createPost" permission
         $createPost = $auth->createPermission('createPost');
         $createPost->description = 'Create a post';
         $auth->add($createPost);

         // add "updatePost" permission
         $updatePost = $auth->createPermission('updatePost');
         $updatePost->description = 'Update post';
         $auth->add($updatePost);*/
    }
}
