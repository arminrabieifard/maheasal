<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tell_us".
 *
 * @property string $id
 * @property string $telegram
 * @property string $instagram
 * @property string $facebook
 * @property string $tells
 * @property string $address
 * @property string $position_y
 */
class TellUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tell_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'position_y'], 'number'],
            [['telegram', 'instagram', 'facebook', 'tells','address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telegram' => 'آدرس تلگرام',
            'instagram' => 'آدرس اینستاگرام',
            'facebook' => 'آدرس فیسبوک',
            'tells' => 'تلفن ها',
            'address' => 'آدرس',
            'position_y' => 'نقطه y نقشه',
        ];
    }
}
