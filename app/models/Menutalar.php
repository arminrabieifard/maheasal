<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%menutalar}}".
 *
 * @property string $id
 * @property string $title عنوان
 * @property string $menu_id مربوط به منو
 * @property string $description توضیحات
 * @property string $file_name نام فایل
 * @property int $num_view تعداد بازدید
 * @property int $visible نمایش داده شود؟
 * @property int $create_at زمان افزودن
 *
 * @property MenuCategory $menu
 */
class Menutalar extends \yii\db\ActiveRecord
{
    public $statusArr = [
        0 => 'خیر',
        1 => 'بله',
    ];
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menutalar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'menu_id', 'description'], 'required'],
            [['menu_id', 'num_view', 'visible', 'create_at'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['file_name'], 'string', 'max' => 50],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuCategory::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'عنوان',
            'menu_id' => 'مربوط به منو',
            'description' => 'توضیحات',
            'file_name' => 'نام فایل',
            'num_view' => 'تعداد بازدید',
            'visible' => 'ویژه نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(MenuCategory::className(), ['id' => 'menu_id']);
    }
    public function getMenuCategoryList()
    {
        return ArrayHelper::map(MenuCategory::findAll(['visible' => 1]), 'id', 'title');
    }
}
