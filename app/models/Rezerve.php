<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%rezerve}}".
 *
 * @property string $id
 * @property string $full_name نام و نام خانوادگی
 * @property string $email ایمیل
 * @property int $date تاریخ رزرو
 * @property string $message متن پیام
 * @property string $tells jgtk
 * @property string $ip آی پی
 * @property int $status وضعیت
 * @property int $visible نمایش داده شود؟
 * @property int $create_at زمان افزودن
 */
class Rezerve extends \yii\db\ActiveRecord
{
    public $vCode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rezerve}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'date', 'message', 'tells'], 'required'],
            [['status', 'visible', 'create_at'], 'integer'],
            [['email'], 'email'],
            [['message'], 'string'],
            [['full_name', 'date', 'email', 'tells'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 45],
            ['vCode','captcha','captchaAction' => 'site/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'نام و نام خانوادگی',
            'email' => 'ایمیل',
            'date' => 'تاریخ رزرو',
            'message' => 'متن پیام',
            'tells' => 'تلفن',
            'ip' => 'آی پی',
            'status' => 'وضعیت',
            'visible' => 'نمایش داده شود؟',
            'create_at' => 'زمان افزودن',
            'vCode' => 'کد تایید',
        ];
    }
}
