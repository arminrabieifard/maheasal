<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property string $id
 * @property string $title
 * @property string $file_name
 * @property integer $visible
 */
class Slider extends \yii\db\ActiveRecord
{
    public $statusArr = [
        0 => 'خیر',
        1 => 'بله',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['visible'], 'integer'],
            [['title', 'file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'عنوان',
            'file_name' => 'نام فایل',
            'visible' => 'نمایش داده شود؟',
        ];
    }
}
