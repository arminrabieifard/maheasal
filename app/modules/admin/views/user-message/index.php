<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'پیام کابران';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="user-message-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'full_name',
                        'email:email',
                        'message:ntext',
                        //'ip',
                        // 'status',
                        //'create_at',
                        [
                            'attribute' => 'create_at',
                            'format' => 'raw',
                            'content' => function($data){
                                return \app\components\General::persianDate($data->create_at);
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {reply} {delete}',
                                'buttons' => [
                                'reply' => function ($url, $model, $key) {
                                    return Html::a(
                                        '<span class="fa fa-reply-all"></span>',
                                        ['reply-to-message/create', 'id' => $model->id],
                                        [
                                            'title' => 'پاسخ به این پیام',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

