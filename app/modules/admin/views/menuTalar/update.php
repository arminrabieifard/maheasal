<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuTalar */

$this->title = 'Update Menu Talar: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Menu Talars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-talar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
