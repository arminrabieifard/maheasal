<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuTalar */

$this->title = 'Create Menu Talar';
$this->params['breadcrumbs'][] = ['label' => 'Menu Talars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-talar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
