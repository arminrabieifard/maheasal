<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReplyToMessage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="reply-to-message-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="form-group">
                <lable></lable>
                <input type="text" class="form-control" value="<?= Html::encode($messageModel->email)?>" disabled>
            </div>

            <?= $form->field($model, 'reply')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
