<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-6">


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>





    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>
    <div class="col-sm-6">
        <?php
        $role = $model->getAllRoles();
        foreach ($role as $k => $v):
        ?>
        <div class="panel panel-primary">
            <div class="panel-heading"><?= $k;?></div>
            <div class="panel-body">
                <?php foreach ($v as $item){
                    echo Html::checkbox("Items[{$item['name']}]" ,$item['checked'] , ['label' => $item['label']]);
                }?>
            </div>
        </div>
        <?php endforeach;?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>