<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $imagesModel app\modules\admin\models\ImagesUploader */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="gallery-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'category_id')->dropDownList($model->getCategoryList(), ['prompt' => 'دسته بندی مورد نظر را انتخاب کنید...']) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'visible')->checkbox() ?>
            <?= $form->field($imageModel, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

            <?php if(!$model->isNewRecord) :?>
                
                <?php if($model->images_file != null):?>
                    <div class="row">
                        <?php foreach (json_decode($model->images_file, true) as $item => $value): ?>

                                <div class="col-xs-3">
                                    <div class="img">
                                        <img src="<?= Yii::getAlias('@uploads-url') . '/gallery/' . Html::encode($value)?>"  style="width: 200px; height: 200px; object-fit: cover">
                                    </div>
                                    <div class="options">
                                        <button class="alert alert-danger" onclick="confirm('آیا اطمینان از حذف این مورد دارید؟')">حذف</button>
                                    </div>
                                </div>

                        <?php endforeach;?>
                    </div>

                <?php endif;?>
                

                
            <?php endif;?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
