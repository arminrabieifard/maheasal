<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuTalar */

$this->title = 'ویرایش منو تالار';
$this->params['breadcrumbs'][] = ['label' => 'منو تالار', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ویرایش';
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
<div class="menu-talar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'imageModel' => $imageModel
    ]) ?>

</div>
    </div></div>
