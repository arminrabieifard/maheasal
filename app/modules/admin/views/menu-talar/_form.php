<?php

use app\components\General;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

/* @var $model app\models\MenuTalar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
<div class="menu-talar-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'menu_id')->dropDownList($model->getMenuCategoryList(), ['prompt' => 'دسته بندی مورد نظر را انتخاب کنید...']) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'visible')->checkbox() ?>
    <?= $form->field($imageModel, 'imageFile')->fileInput() ?>
    <div class="form-group">
        <label for="">متن </label>

        <?= \dosamigos\tinymce\TinyMce::widget([
            'name' => 'Menutalar[description]',
            'value' => $model->description,
            'language' => 'fa',
            'clientOptions' => General::getTinyMceConfig(),
        ]);
        ?>
    </div>



    <?php if(!$model->isNewRecord) :?>

        <?= $form->field($model, 'visible')->checkbox() ?>

    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>

