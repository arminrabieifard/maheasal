<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\HtmlPurifier;
/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'خبرها', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="news-view">

            <p>
                <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',

                    'description:ntext',
                    [
                        'attribute' => 'description',
                        'format' => 'raw',
                        'value' => HtmlPurifier::process($model->description),
                    ],
                    //   'file_name',
                    [
                        'attribute' => 'file_name',
                        'format'=>'html',
                        'value' => function ($data) {

                            return Html::img(Yii::getAlias('@storage-url').'/image/front/news/' . $data->file_name,['width' => '150px']);
                        },
                    ],
                    //  'num_view',
                    //'create_at',
                    [
                        'attribute' => 'create_at',
                        'format' => 'raw',
                        'value' => \app\components\General::persianDate($model->create_at),
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>

