<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rezerve */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rezerves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
<div class="rezerve-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'email:email',
            'date',
            'message:ntext',
            'tells',
            'ip',
            'status',
            'visible',
            'create_at',
        ],
    ]) ?>

</div>
    </div>
</div>