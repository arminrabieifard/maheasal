<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rezerve */

$this->title = 'Create Rezerve';
$this->params['breadcrumbs'][] = ['label' => 'Rezerves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rezerve-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
