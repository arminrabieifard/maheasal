<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rezerve */

$this->title = 'Update Rezerve: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Rezerves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rezerve-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
