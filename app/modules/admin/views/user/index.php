<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ادمین';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

  <!--  <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
        <li><a data-toggle="tab" href="#menu1">role</a></li>

    </ul>-->

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="box-body">

                <div class="user-index">

                    <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">مدیران</a></li>
                    <li><a data-toggle="tab" href="#menu1">کاربران</a></li>
                    </ul>
                    <?php Pjax::begin(); ?>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <p>
                                <?= Html::a('افزودن ادمین', ['create'], ['class' => 'btn btn-success']) ?>
                            </p>
                            <?= GridView::widget([
                                'dataProvider' => $admindataProvider,
                                'columns' => [
                                    //['class' => 'yii\grid\SerialColumn'],

                                    'id',
                                    'full_name',
                                    'user_name',
                                    //'password',
                                    //'auth_key',
                                    // 'password_reset_token',
                                    // 'email:email',
                                    'active',
                                    [

                                        'attribute' => 'auth_item',
                                        'content' => function($data){
                                            $d=\app\models\AuthAssignment::findOne(['user_id' => $data->id]);
                                            if($d!=null){
                                                return $d->item_name;
                                            }else{

                                            }

                                        }
                                    ],
                                    // 'create_at',

                                    [
                                        'attribute' => 'create_at',
                                        'format' => 'raw',
                                        'content' => function($data){
                                            return \app\components\General::persianDate($data->create_at);
                                        }
                                    ],

                                    ['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <p>
                                <?= Html::a('افزودن کاربر', ['user-create'], ['class' => 'btn btn-success']) ?>
                            </p>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    //['class' => 'yii\grid\SerialColumn'],

                                    'id',
                                    'full_name',
                                    'user_name',
                                    //'password',
                                    //'auth_key',
                                    // 'password_reset_token',
                                    // 'email:email',
                                    'active',
                                    [

                                        'attribute' => 'auth_item',
                                        'content' => function($data){
                                            $d=\app\models\AuthAssignment::findOne(['user_id' => $data->id]);
                                            if($d!=null){
                                                return $d->item_name;
                                            }else{

                                            }

                                        }
                                    ],
                                    // 'create_at',

                                    [
                                        'attribute' => 'create_at',
                                        'format' => 'raw',
                                        'content' => function($data){
                                            return \app\components\General::persianDate($data->create_at);
                                        }
                                    ],

                                    ['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        </div>

                    </div>



                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    <!--    <div id="menu1" class="tab-pane fade">
            <div class="box-body">

                <div class="user-index">
                    <p>
                        <?/*= Html::a('افزودن نقش', ['role/create'], ['class' => 'btn btn-success']) */?>
                    </p>
                    <?php /*Pjax::begin(); */?>    <?/*= GridView::widget([
                        'dataProvider' => $dataProviderRole,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                           // 'id',
                            'name',
                           // 'type',
                            //'password',
                            //'auth_key',
                            // 'password_reset_token',
                            // 'email:email',
                            'description',


                          //  'created_at',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'controller' => 'role',

                            ],
                        ],
                    ]); */?>
                    <?php /*Pjax::end(); */?>
                    role
                </div>
            </div>
        </div>-->

    </div>


    </div>
