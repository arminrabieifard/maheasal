<?php

namespace app\modules\admin;
use app\models\AuthAssignment;
use yii\web\HttpException;

/**
 * admin module definition class
 */
class AdminModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    public function beforeAction($action)
    {
        $id = \Yii::$app->user->id;
        if($id != null){
            $user = AuthAssignment::find()->where(['user_id' => $id])->one();
            if ($user->item_name === 'USer' ){

                throw new HttpException(403, 'You are not an admin');
                return false;
            }else{
                parent::init();
            }
        }

        return true;
    }
}
