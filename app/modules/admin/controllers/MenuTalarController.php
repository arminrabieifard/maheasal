<?php

namespace app\modules\admin\controllers;


use app\components\General;
use app\modules\admin\models\ImageUploader;
use Yii;
use app\models\MenuTalar;
use app\models\MenuTalarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MenuTalarController implements the CRUD actions for MenuTalar model.
 */
class MenuTalarController extends CustomController
{

    public function actionIndex()
    {
        $searchModel = new MenuTalarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MenuTalar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenuTalar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MenuTalar();
        $img = new ImageUploader();
        $img->scenario = 'create';
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {

            $imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile');
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;

            if($result = $imageModel->upload($picName, 'menutalar', 700))
            {
                $model->file_name = $picName;
                $model->create_at = time();

                if($model->save())
                {
                    Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                    return $this->redirect(['site-map/index','section' => 'menu-talar', 'page' => 'view' ,'id' => $model->id]);
                }
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['danger', General::showSummaryError($result)]);
                return $this->render('create', [
                    'model' => $model,
                    'imageModel' => $img,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'imageModel' => $img,
            ]);
        }
        }

    /**
     * Updates an existing MenuTalar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageModel = new ImageUploader();
        $imageModel->scenario = 'update';

        if(($imageModel->imageFile = UploadedFile::getInstance($imageModel,'imageFile')) !== null)
        {
            $picName = $imageModel->imageFile->baseName . '_' . Yii::$app->security->generateRandomString(5) . time() . '.' . $imageModel->imageFile->extension;
            if($result = $imageModel->updateUpload($picName, 'menutalar', $model->file_name, 700))
            {
                $model->file_name = $picName;
            }
            else
            {
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            return $this->render('update', [
                'model' => $model,
                'imageModel' => $imageModel,
            ]);
        }
    }
    /**
     * Deletes an existing MenuTalar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        @unlink(Yii::getAlias('@uploads-root') . '/news/' . $model->file_name);
        $model->delete();
        Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the MenuTalar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MenuTalar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenuTalar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
