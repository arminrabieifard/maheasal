<?php

namespace app\modules\admin\controllers;

use app\models\AuthAssignment;
use app\models\Role;
use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends CustomController
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $adminID = ArrayHelper::getColumn(AuthAssignment::find()->select(['user_id'])->where(['item_name' => 'Admin'])->all(), 'user_id');
        //AuthAssignment::find()->where(['item_name' => 'USer'])->all();
        $user = User::find()->where(['id' => $adminID])->all();

        $admindataProviderRole = new ActiveDataProvider([
            'query' => User::find()->where(['id' => $adminID]),
        ]);

        $userID = ArrayHelper::getColumn(AuthAssignment::find()->select(['user_id'])->where(['item_name' => 'USer'])->all(), 'user_id');
        //AuthAssignment::find()->where(['item_name' => 'USer'])->all();
        $user = User::find()->where(['id' => $userID])->all();

        $dataProviderRole = new ActiveDataProvider([
            'query' => User::find()->where(['id' => $userID]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProviderRole,
            'admindataProvider' => $admindataProviderRole,

        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $model->active = 1;
            $model->create_at = time();
            if($model->save())
            {
                echo $model->password;
                exit();
                $model->save_assignment($model->id , $model->auth_item);
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionUserCreate(){
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $model->active = 1;
            $model->create_at = time();
            if($model->save())
            {
                $model->save_assignment($model->id , $model->auth_item);
                echo $model->password;
                exit();
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                print_r($model->getErrors());
            }
        } else {
            return $this->render('user-create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save_assignment($id , $model->auth_item);
            Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $assign = AuthAssignment::findOne(['user_id' => $id]);
        if($assign!= null){
            $assign->delete();
        }

        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
