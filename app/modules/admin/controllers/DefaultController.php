<?php

namespace app\modules\admin\controllers;

use app\models\AuthAssignment;
use app\modules\admin\models\LoginForm;
use Yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends CustomController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionLogin(){

        $this->layout='login';

        if(!Yii::$app->user->isGuest)
        {
            return $this->redirect(['index']);
        }

        $model = new LoginForm();

        if($model->load(Yii::$app->request->post()) && $model->loginAdmin())
        {
            $user_id =  Yii::$app->user->id;
            $item = AuthAssignment::findOne(['user_id' => $user_id]);

            if($item->item_name == 'Admin'){
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['../site']);
            }


        }
        return $this->render('login',[
            'model' => $model
        ]);
    }
   /* public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();

        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user_id =  Yii::$app->user->id;
            $item = AuthAssignment::findOne(['user_id' => $user_id]);
            echo $item->item_name;
            exit();
            if($item->item_name == 'Admin'){
                return $this->goBack();
            }else{
                return $this->goHome();
            }

        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }*/

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
