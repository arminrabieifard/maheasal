<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085758_tell_us extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%tell_us}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'telegram'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس تلگرام'),
                'instagram'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس اینستاگرام'),
                'facebook'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس فیسبوک'),
                'email'=> $this->string(255)->null()->defaultValue(null)->comment('آدرس ایمیل'),
                'position_x'=> $this->decimal(15, 5)->null()->defaultValue(null)->comment('نقطه x نقشه'),
                'position_y'=> $this->decimal(15, 5)->null()->defaultValue(null)->comment('نقطه y نقشه'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%tell_us}}');
    }
}
