<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_category`.
 */
class m180301_181443_create_menu_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%menu_category}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'parent_id'=> $this->integer(11)->unsigned()->null()->defaultValue(null)->comment('زیر شاخه دسته'),
                'title'=> $this->string(50)->notNull()->comment('عنوان'),

                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('idx-category-parent_id','{{%menu_category}}',['parent_id'],false);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropIndex('idx-category-parent_id', '{{%menu_category}}');
        $this->dropTable('{{%menu_category}}');
    }
}
