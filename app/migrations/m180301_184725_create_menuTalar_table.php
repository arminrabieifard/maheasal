<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menuTalar`.
 */
class m180301_184725_create_menuTalar_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%menuTalar}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'title'=> $this->string(100)->notNull()->comment('عنوان'),
                'menu_id'=> $this->integer(11)->unsigned()->notNull()->comment('مربوط به منو'),
               'description'=> $this->text()->notNull()->comment('توضیحات'),
                'file_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام فایل'),
                'num_view'=> $this->integer(11)->null()->defaultValue(0)->comment('تعداد بازدید'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->addForeignKey('fk_menuTalar_menu_id',
            '{{%menuTalar}}','menu_id',
            '{{%menu_category}}','id',
            'CASCADE','CASCADE'
        );
        $this->createIndex('idx-gallery-menu_id','{{%menuTalar}}',['menu_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('idx-gallery-menu_id', '{{%menuTalar}}');
        $this->dropTable('{{%menuTalar}}');
        $this->dropForeignKey('fk_menuTalar_menu_id', '{{%menuTalar}}');
    }
}
