<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rezerve`.
 */
class m180320_164431_create_rezerve_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
        '{{%rezerve}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'full_name'=> $this->string(255)->notNull()->comment('نام و نام خانوادگی'),
                'email'=> $this->string(255)->notNull()->comment('ایمیل'),
                'date'=> $this->integer()->notNull()->comment('تاریخ رزرو'),
                'message'=> $this->text()->notNull()->comment('متن پیام'),
                'tells'=> $this->string(255)->notNull()->comment('jgtk'),
                'ip'=> $this->string(45)->null()->defaultValue(null)->comment('آی پی'),
                'status'=> $this->smallInteger(1)->unsigned()->null()->defaultValue(0)->comment('وضعیت'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rezerve');
    }
}
