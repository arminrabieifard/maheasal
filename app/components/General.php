<?php


namespace app\components;


use Yii;
use alexantr\elfinder\TinyMCE;
use yii\base\Component;
use IntlDateFormatter;


class General extends Component
{
    public static function getMin($filter, $data, $isFloat = false)
    {
        if(isset($data))
        {
            if($filter < $data)
            {
                $data = $filter;
            }
        }
        else
        {
            $data = $filter;
        }

        return  ($isFloat ? (float) $data : $data);
    }

    public static function getMax($filter, $data, $isFloat = false)
    {
        if(isset($data))
        {
            if($filter > $data)
            {
                $data = $filter;
            }
        }
        else
        {
            $data = $filter;
        }

        return  ($isFloat ? (float) $data : $data);
    }

    public static function showDate($time, $type)
    {
        switch($type)
        {
            case 2:
                $date = self::persianDate($time, 'EEEE dd LLLL yyyy', 'fa-IR');
                break;
            case 3:
                $date = self::persianDate($time, 'EEEE yyyy/MM/dd', 'fa-IR');
                break;
            case 4:
                $date = self::persianDate($time, 'yy/MM/dd', 'fa-IR');
                break;
            case 5:
                $date = date('Y/m/d', $time);
                break;
            case 6:
                $date = date('D d F Y', $time);
                break;
            case 7:
                $date = date('d F Y', $time);
                break;
            default:
                $date = '';
        }

        return $date;
    }

    public static function showTime($time, $type)
    {
        switch($type)
        {
            case 1:
                $times = self::persianDate($time, 'HH:mm', 'fa-IR');
                break;
            case 2:
                $times = self::persianDate($time, 'h:mm a', 'fa-IR');
                break;
            case 3:
                $times = self::persianDate($time, 'h a', 'fa-IR');
                break;
            case 4:
                $times = self::persianDate($time, 'HH', 'fa-IR');
                break;
            case 5:
                $times = date('g:i A', $time);
                break;
            case 6:
                $times = date('g A', $time);
                break;
            default:
                $times = '';
        }

        return $times;
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function slug($string)
    {
        return str_replace(' ', '-', $string);
    }

    /**
     * @param null $timestamp
     * @param string $format
     * @param string $calenderLang
     * @return string
     */
    public static function persianDate($timestamp = null, $format = 'yyyy/MM/dd', $calenderLang = 'en-US')
    {
        if($timestamp == null) $timestamp = time();
        $fmt = datefmt_create($calenderLang."@calendar=persian", \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Asia/Tehran', \IntlDateFormatter::TRADITIONAL, $format);
        return $fmt->format($timestamp);
    }

    /* no use yet */
    public static function persianMonth()
    {
        $fmt = datefmt_create("fa@calendar=persian", \IntlDateFormatter::FULL, \IntlDateFormatter::NONE, 'Asia/Tehran', \IntlDateFormatter::TRADITIONAL, 'MMMM');
        echo    $fmt->format(time());
    }

    public static function dateTimeFormat($time = null)
    {
        if($time == null) $time = time();
        return date("Y-m-d H:i:s", $time);
    }

    public static function in_array_r($needle, $haystack)
    {
        if($haystack != null)
        {
            foreach($needle as $item)
            {
                if(in_array($item, $haystack))
                {
                    return true;
                    break;
                }
            }
        }

        return false;
    }

    public static function clearCache($blogId)
    {
        //$now = time();

        //Blog::updateAll(['updated_at' => $now], ['id' => $blogId]);
    }

    public static function timeAgo($time)
    {
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'سال',
            2592000 => 'ماه',
            604800 => 'هفته',
            86400 => 'روز',
            3600 => 'ساعت',
            60 => 'دقیقه',
            1 => 'ثانیه'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.' پیش';
        }
    }

    /* Joda Karadane Kalamate Chand Silabi Mesle : GetUserComments => Get User Comments */
    public static function explodePolysyllabicWord($camel, $glue='-')
    {
        $string = preg_replace('/([a-z0-9])([A-Z])/', "$1$glue$2", $camel);

        return strtolower($string);
    }

    public static function showSummaryError($model)
    {
        $error = '';
        foreach($model->errors as $field => $errors)
        {
            foreach($errors as $err)
            {
                $error .= $err."\n";
            }
        }

        return $error;
    }

    public static function showSummaryErrors($model)
    {
        $error = '';
        foreach($model as $array) {
            foreach ($array as $field => $errors) {
                foreach ($errors as $err) {
                    $error .= $err . "\n";
                }
            }
        }
        return $error;
    }

    /**
     * @param $elements
     * @param int $parentId
     * @return array
     */
    public static function flat2nested($elements, $parentId = 0)
    {
        $branch = [];

        foreach($elements as $element)
        {
            if ($element['parent_id'] == $parentId)
            {
                $children = self::flat2nested($elements, $element['id']);
                if ($children)
                {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * @param $data
     * @param string $template
     * @return string
     */
    public static function nested2ul($data, $template)
    {
        $result = [];

        if(sizeof($data) > 0)
        {
            //$result[] = '<ul>';

            foreach($data as $entry)
            {
                $result[] = sprintf(
                    $template,
                    $entry['view_id'],
                    $entry['icon'],
                    $entry['title'],
                    (isset($entry['children'])) ? '<ul>'.self::nested2ul($entry['children'], $template).'</ul>' : null
                );
            }
            //$result[] = '</ul>';
        }

        return implode($result);
    }

    /**
     * @param $models
     * @param string $template
     * @return string
     */
    public static function createTree($models, $template = '<li>(%d) %s %s</li>')
    {
        return self::nested2ul(self::flat2nested($models), $template);
    }

    public static function setFileName($directory, $baseName, $extension)
    {
        $i = 0;
        $baseName = preg_replace("/[^a-z0-9-]/", "", strtolower($baseName));

        # Check and Set File Name When exists with this Name
        do {
            $diff = ($i == 0) ? '' : '_' . $i;
            $fileName = $baseName . $diff . '.' . $extension;
            $i++;
        } while (file_exists($directory . $fileName));

        return $fileName;
    }

    public static function utf8SubStr($str,$start)
    {
        preg_match_all("/./u", $str, $ar);
        if(func_num_args() >= 3)
        {
            $end = func_get_arg(2);
            return join("",array_slice($ar[0],$start,$end));
        }
        else
        {
            return join("",array_slice($ar[0],$start));
        }
    }

    public static function getTinyMceConfig()
    {
        return [
            'menubar' => false,
            'directionality' => 'rtl',
            'language' => 'fa',
            'fontsize_formats' => '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
            'toolbar' => 'undo redo | fontsizeselect fontselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor | link image | table | preview | fullscreen',
            'plugins' => 'image fullscreen link textcolor table preview',
            // ...
            'file_picker_callback' => TinyMCE::getFilePickerCallback(['/el-finder/tinymce']),
            'height' => 250
        ];
    }

    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  Passed to function:                                                    :*/
    /*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
    /*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
    /*::    unit = the unit you desire for results                               :*/
    /*::           where: 'M' is statute miles (default)                         :*/
    /*::                  'K' is kilometers                                      :*/
    /*::                  'N' is nautical miles                                  :*/
    /*::  Worldwide cities and other features databases with latitude longitude  :*/
    /*::  are available at http://www.geodatasource.com                          :*/
    /*::                                                                         :*/
    /*::  For enquiries, please contact sales@geodatasource.com                  :*/
    /*::                                                                         :*/
    /*::  Official Web site: http://www.geodatasource.com                        :*/
    /*::                                                                         :*/
    /*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
    /*::                                                                         :*/
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }

    }

    /** Generate Random Number */
    public static function generateRandomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public static function pdf($size, $orientation)
    {
        Yii::$app->response->format = 'pdf';

        // Rotate the page
        Yii::$container->set(Yii::$app->response->formatters['pdf']['class'], [
            'format' => [216, 356], // Legal page size in mm
            'orientation' => 'Landscape', // This value will be used when 'format' is an array only. Skipped when 'format' is empty or is a string
            'beforeRender' => function($mpdf, $data) {},
        ]);
    }

    public static function convertMultipleDetermine2Single($arrays, $field = 'hotelId', $unique = true, $needKey = false)
    {
        $objTmp = (object) array($field => array());

        array_walk_recursive($arrays, create_function('&$v, $k, &$t', ($needKey ? '$t->'.$field.'[$k] = $v;' : '$t->'.$field.'[] = $v;')), $objTmp);

        return ($unique ? array_unique($objTmp->$field) : $objTmp->$field);
    }

    /**
     * Convert Persian Date To Gregorian Date
     * @param $startDate
     * @param string $delimiter
     * @return string
     */
    public static function toGregorianDate($startDate, $delimiter = '/')
    {
        $persianDateInstance = \IntlCalendar::createInstance('Asia/Tehran', 'fa_IR@calendar=persian');
        $intlDateFormatter   = new IntlDateFormatter("en_US", IntlDateFormatter::FULL, IntlDateFormatter::FULL, 'Asia/Tehran', IntlDateFormatter::GREGORIAN, 'yyyy/MM/dd');

        $startDateExplode = explode($delimiter, $startDate);
        $startDateYear    = (int) $startDateExplode[0];
        $startDateMonth   = ((int) $startDateExplode[1]) - 1;
        $startDateDay     = (int) $startDateExplode[2];

        $persianDateInstance->set($startDateYear, $startDateMonth, $startDateDay);// Notice that month number begin from 0 not 1.

        return $intlDateFormatter->format($persianDateInstance);
    }

    /**
     * Convert Gregorian Date To Persian Date
     * @param $startDate
     * @param string $delimiter
     * @return string
     */
    public static function toPersianDate($startDate, $delimiter = '/')
    {
        $startDateExplode = explode($delimiter, $startDate);
        $startDateYear    = (int) $startDateExplode[0];
        $startDateMonth   = ((int) $startDateExplode[1]) - 1;
        $startDateDay     = (int) $startDateExplode[2];

        $date = \IntlCalendar::createInstance(
            'Asia/Tehran',
            'fa_IR@calendar=persian'
        );

        $date->set($startDateYear, $startDateMonth, $startDateDay); // Notice that month number begin from 0 not 1.

        $intlDateFormatter = new IntlDateFormatter(
            "en_US", // string $locale
            IntlDateFormatter::FULL, // int $datetype
            IntlDateFormatter::FULL, // int $timetype
            'Asia/Tehran', // mixed $timezone
            IntlDateFormatter::GREGORIAN, // mixed $calendar
            'yyyy/MM/dd' // string $pattern
        );

        return $intlDateFormatter->format($date);
    }

    public static function isEmpty($value)
    {
        return $value === '' || $value === [] || $value === null || is_string($value) && trim($value) === '';
    }

    public static function getCookieAuthKey()
    {

        if (Yii::$app->request->cookies->has('_identity')) {
            $data = json_decode(Yii::$app->request->cookies->getValue('_identity'), true);
            if (count($data) == 3) {
                list ($id, $authKey, $duration) = $data;

            } else {
                $authKey = null;
            }

            return $authKey;
        }
    }

    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
