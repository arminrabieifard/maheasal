<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/20/2018
 * Time: 8:23 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = "پیش رزرو آنلاین تالار ماه عسل";

?>
<div class="clearfix"></div>
 <div class="container comment">
<ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">فرم پیش رزرو</a></li>


    </ul>

 <?php
    if (Yii::$app->session->hasFlash('alert')) {
        list($icon, $message) = Yii::$app->session->getFlash('alert');

        echo \yii\bootstrap\Alert::widget(['options' => [
            'class' => 'alert-' . $icon,
        ],
            'body' => $message,
        ]);
    }

    ?>
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => 'true',
    ]); ?>
     <div class="user-message-form col-sm-6">
     <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'date')->widget(jDate\DatePicker::className()) ?>
    <?= $form->field($model, 'vCode')->widget(\yii\captcha\Captcha::className(), ['captchaAction' => '/site/captcha']) ?>
     </div>
     <div class="user-message-form col-sm-6">

     <?= $form->field($model, 'tells')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     </div>
    <?php ActiveForm::end(); ?>


 </div>