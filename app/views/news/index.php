<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'لیست خبرها';
?>
<div class="clearfix"></div>

    <div class="main   col-sm-12">
        <div class="news">
            <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class="">آخرین اخبار</h2>

        </div>


    </div>
<div class="row">
    <div class="news-page">
    <?php foreach ($model as $new):?>
        <div class="col-sm-5 news-item">
            <a href="<?= Url::to(['view', 'id' => $new->id])?>">
                <div class="col-sm-6 img">
                    <img class="img-responsive" src="<?= Yii::getAlias('@web/storage') . '/image/front/news/' . $new->file_name ?>" alt="<?= Html::encode($new->title) ?>">
                </div>
                <div class="col-sm-6 news-item-body">
                <div class="news-title">
                    <?= Html::encode($new->title) ?>
                </div>
                <span class="des-news">
                      <?= $new->description;?>
                </span>
                <div class="view">
                    <span> تعداد بازدید :</span>
                    <span> <?= $new->num_view;?></span>
                </div>
                </div>
            </a>
        </div>
        <div class="col-sm-1"></div>
    <?php endforeach;?>
    </div>
    <div class="">
        <?php
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
        ?>
    </div>
</div>