<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/6/2018
 * Time: 10:06 PM
 */?>
<div class="clearfix"></div>

<div class="main   col-sm-12">
    <div class="news">
        <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class=""><?=$model->title?></h2>

    </div>


</div>
<div class="row">
    <div class="news-page col-sm-12">
        <div class="news-descrip col-sm-3">
            <div class="col-sm-6 img">
                <img class="img-responsive" src="<?= Yii::getAlias('@web/storage') . '/image/front/news/' . $new->file_name ?>" alt="<?= Html::encode($new->title) ?>">
            </div>
        </div>
        <div class="news-descrip col-sm-9">
            <?= $model->description;?>
        </div>
    </div>
</div>
