<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/6/2018
 * Time: 10:06 PM
 */
use yii\helpers\Html; ?>
<div class="clearfix"></div>
<?php
$model->num_view = $model->num_view *1 +1;
$model->save();
?>
<div class="main   col-sm-12">
    <div class="news">
        <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class=""><?=$model->title?></h2>

    </div>


</div>
<div class="row">
    <div class="news-page col-sm-12">
        <div class="news-descrip col-sm-5">
            <div class="col-sm-7 ">
                <img class="img-responsive" src="<?= Yii::getAlias('@web/storage') . '/image/front/news/' . $model->file_name ?>" alt="<?= Html::encode($model->title) ?>">
            </div>
        </div>
        <div class="news-descrip col-sm-7">
            <?= $model->description;?>
        </div>
    </div>
</div>
