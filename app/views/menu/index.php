<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/2/2018
 * Time: 5:34 PM
 */
$this->title = 'منو تالار ماه عسل ';
?>
<section>
    <?php
    foreach ($category as $items):
    ?>
    <div class="col-sm-12 gallery-list line">
        <div class="col-sm-3">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/menu.png'?>" alt="">
                <div class="title-gallery">
                    <h2>لیست منو</h2>
                    <h3><?= $items->title;?></h3>
                </div>
            </div>
        </div>
        <div class="col-sm-9">

            <ul class="owl-carousel owl-alt-controls " data-autoplay="no" data-pagination="no" data-arrows="yes">
<?php
$menu = \app\models\Menutalar::find()->where(["menu_id" => $items->id , 'visible' => 0])->all();
foreach ($menu as $mentalar):
?>
                <li class="item property-block">
                    <a href="" class="property-featured-image" data-toggle="modal" data-target="#myModal<?=$mentalar->id;?>">
                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/menutalar/'.$mentalar->file_name;?>" alt="">
                    </a>
                    <div class="gitem-title">
                        <h4><?= $mentalar->title;?></h4>
                    </div>
                </li>

    <?php endforeach;?>

            </ul>
        </div>
    </div>
        <?php
        foreach ($menu as $mentalar){
        ?>
            <div id="myModal<?=$mentalar->id;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><?=$mentalar->title;?></h4>
                        </div>
                        <div class="modal-body ">

                                <?=$mentalar->description;?>


                                <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/menutalar/'.$mentalar->file_name;?>" alt="">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                        </div>
                    </div>

                </div>
            </div>
    <?php } endforeach; ?>

</section>
