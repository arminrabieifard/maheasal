<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/2/2018
 * Time: 5:34 PM
 */
$this->title = 'گالری تالار ماه عسل ';
?>
<section>
    <div class="col-sm-12 gallery-list line">
        <div class="col-sm-3">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/gallery.png'?>" alt="">
                <div class="title-gallery">
                    <h2>گالری تصاویر</h2>
                    <h3>بازدید از تصاویر تالار</h3>
                </div>
            </div>
        </div>
        <div class="col-sm-9">

            <ul class="owl-carousel owl-alt-controls " data-autoplay="no" data-pagination="no" data-arrows="yes">
                <?php
                foreach ($category as $items):
                ?>
                    <li class="item property-block">
                        <a href="" class="property-featured-image" data-toggle="modal" data-target="#myModal<?=$items->id;?>">
                            <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$items->file_name;?>" alt="">
                        </a>
                        <div class="gitem-title">
                            <h4><?=$items->title;?></h4>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
    <?php
    foreach ($category as $mentalar){
        ?>
        <div id="myModal<?=$mentalar->id;?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?=$mentalar->title;?></h4>
                    </div>
                    <div class="modal-body ">

                        <?=$mentalar->description;?>


                        <img class="img-responsive img" src="<?= Yii::getAlias('@storage-url').'/image/front/gallery/'.$mentalar->file_name;?>" alt="">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                    </div>
                </div>

            </div>
        </div>
    <?php } ?>

</section>
