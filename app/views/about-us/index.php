<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'درباره ما';
?>
<div class="clearfix"></div>
<section>
    <div class="main   col-sm-12">
        <div class="news">
            <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class=""><?= Html::encode($model->title)?></h2>

        </div>


    </div>
    <div class="news-body new-news-body">


        <?= HtmlPurifier::process($model->description)?>

    </div>

</section>
<section>
    <div class="main col-sm-12">
        <div class="social">
            <a href="<?= $tellmodel->facebook;?>"><span class="icon facebook"></span></a>
            <a href="<?= $tellmodel->telegram;?>"><span class="icon telegram"></span></a>
            <a href="<?= $tellmodel->instagram;?>"><span class="icon insta"></span></a>

        </div>
        <div class="address">
            <div class="icon tell"></div><div class="answer"><?= $tellmodel->tells;?></div><br/>
            <div class="icon map"></div><div class="answer"><?= $tellmodel->address;?></div>
        </div>
    </div>
</section>
