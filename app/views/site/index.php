<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'تالار ماه عسل';
?>
<div class="clearfix"></div>
<section>
    <div class="main line col-sm-12">
        <div class="col-sm-3 col-xs-6">
            <a href="<?=  Url::to(['gallery/' ]);?>">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/gallery.png'?>" alt="">
                <div class="title-gallery">
                    <h2>گالری تصاویر</h2>
                    <h3>بازدید از تصاویر تالار</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?=  Url::to(['/rezerve/' ]);?>">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/register.png'?>" alt="">
                <div class="title-gallery">
                    <h2>پیش رزرو</h2>
                    <h3>جهت رزرو قبل عروسی</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="<?=  Url::to(['menu/' ]);?>">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/menu.png'?>" alt="">
                <div class="title-gallery">
                    <h2> منو غدا</h2>
                    <h3>لیست منوهای تالار</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <a href="">
            <div class="gallery-item">
                <img src="<?= Yii::getAlias('@storage-url').'/image/tour.png'?>" alt="">
                <div class="title-gallery">
                    <h2>تور مجازی</h2>
                    <h3>گردشی در ماه عسل</h3>
                </div>
            </div>
            </a>
        </div>

    </div>

</section>
<div class="space"></div>
<div class="clearfix"></div>
<section>
    <div class="main   col-sm-12">
        <div class="news">
            <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class="">آخرین اخبار تالار</h2>

        </div>


    </div>

    <div class="news-body">
        <?php
        foreach ($newsModel as $item):
            ?>
            <a href="<?= Url::to(["news/view", 'id' => $item->id]);?>"><h3><?= $item->title;?></h3></a>
            <?php
        endforeach;
        ?>


    </div>

</section>
<div class="clearfix"></div>
<div class="space"></div>
<section>
    <div class="main   col-sm-12">
        <div class="news">
            <span class=""> <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt=""></span><h2 class="">آخرین نظرات کاربران</h2>

        </div>


    </div>
    <div class="news-body">
        <?php
            foreach ($pmModel as $item):
        ?>
        <a href="<?= Url::to("comment/");?>"><h3><?= $item->message;?></h3></a>
        <?php
        endforeach;
        ?>
    </div>

</section>



