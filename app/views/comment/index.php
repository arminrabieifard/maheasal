<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/6/2018
 * Time: 6:14 PM
 */

/* @var $model */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="clearfix"></div>
<div class="container comment">
    <?php
    if (Yii::$app->session->hasFlash('alert')) {
        list($icon, $message) = Yii::$app->session->getFlash('alert');

        echo \yii\bootstrap\Alert::widget(['options' => [
            'class' => 'alert-' . $icon,
        ],
            'body' => $message,
        ]);
    }

    ?>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">نظرات کاربران</a></li>
        <li><a data-toggle="tab" href="#menu1">فرم ارسال نظر</a></li>

    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">

                <div class="main   col-sm-12">
                    <div class="news">
                        <span class="">
                            <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt="">
                        </span>
                        <h2 class="">علی مرادی</h2>
                        <p class="description-tab">


                            لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.

                            توسط فرم زیر می توانید متن ساختگی مورد نظر خود را در واحدهای کاراکتر، کلمه و پاراگراف تولید کنید، سپس آنرا کپی کنید و در کار مورد نظر خود قرار دهید.
                            زبان مورد نظر خود را انتخاب کنید ...
                            تعداد را انتخاب کنید
                            تولیدکننده لورم ایپسوم فارسی :: سرویسی رایگان از Webtaz | تبليغات در سايت

                        </p>
                    </div>
                </div>
            <div class="main   col-sm-12">
                <div class="news">
                        <span class="">
                            <img src="<?= Yii::getAlias('@storage-url').'/image/icon.png'?>" alt="">
                        </span>
                    <h2 class="">علی مرادی</h2>
                    <p class="description-tab">


                        لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.

                        توسط فرم زیر می توانید متن ساختگی مورد نظر خود را در واحدهای کاراکتر، کلمه و پاراگراف تولید کنید، سپس آنرا کپی کنید و در کار مورد نظر خود قرار دهید.
                        زبان مورد نظر خود را انتخاب کنید ...
                        تعداد را انتخاب کنید
                        تولیدکننده لورم ایپسوم فارسی :: سرویسی رایگان از Webtaz | تبليغات در سايت

                    </p>
                </div>
            </div>



        </div>
        <div id="menu1" class="tab-pane fade">
            <div class="user-message-form col-sm-6">

                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => 'true',
                ]); ?>

                <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'vCode')->widget(\yii\captcha\Captcha::className(), ['captchaAction' => '/site/captcha']) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'افزودن' : 'ویرایش', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</div>
