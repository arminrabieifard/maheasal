<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Slider;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php

    $this->registerCssFile('@web/storage/css/general/style.less');

    ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="">
        <div class="container-fluid">
            <div class="head col-sm-12 col-xs-12  navbar navbar-inverse">
                <nav class="col-sm-12 col-xs-12 " role="navigation">
                    <div class="col-xs-6 ">
                        <div class="navbar-header padd-head">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                    <div class="collapse padd-head col-sm-8 navbar-collapse col-xs-12" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav ">
                            <li class=" "><a href="<?=  Url::to(['/' ]);?>" class="select" >صفحه اصلی</a></li><span></span>
                            <li class=""><a href="#"> تورمجازی</a></li><span></span>
                            <li class=""><a href="<?=  Url::to(['/menu' ]);?>">لیست منو</a></li><span></span>
                            <li class=""><a href="<?=  Url::to(['/gallery' ]);?>" >گالری تصاویر</a></li><span></span>
                            <li class=""><a href="<?=  Url::to(['/comment' ]);?>" >نظرات</a></li><span></span>
                            <li class=""><a href="<?=  Url::to(['/news' ]);?>"> اخبار</a></li><span></span>
                            <li class=" boredr-none"><a href="<?=  Url::to(['/about-us' ]);?>"> درباره ما</a></li>


                        </ul>
                    </div>
                    <div class="collapse padd-head col-sm-4 navbar-collapse col-xs-12" id="bs-example-navbar-collapse-1">
                        <ul>
                            <?php
                            if( Yii::$app->user->getId() != null){
                                $user = Yii::$app->user->getId();

                                if($user != null){
                                    $roles = \app\models\AuthAssignment::findOne(['user_id' => $user]);
                                    if($roles->item_name == 'USer'){
                                        ?>
                                        <ul class="nav navbar-nav nav2 ">
                                            <li class="active"><a href="#"></a></li>
                                            <li class="dropdown boredr-none">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= Yii::$app->user->identity->full_name; ?>
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li class=" boredr-none"><?= Html::a('خروج', Url::to(['site/logout']), ['data-method' => 'POST']) ?>  </li>
                                                    <li class=" boredr-none"><a href="<?=  Url::to(['menu/important' ]);?>">  منوی ویژه</a></li>
                                                    <li class=" boredr-none"><a href="<?=  Url::to(['gallery/important' ]);?>">  گالری ویژه</a></li>
                                                </ul>
                                            </li>

                                        </ul>

                                        <?php
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </nav>


            </div>

        </div>
        <div class="logo-bar">

        </div>
    </div>
<div class="slider">
    <div class="slider col-sm-12 col-xs-12">
        <?php
        $sliderModel = Slider::find()->where(['visible' => 1])->orderBy(['id' => SORT_DESC])->all();
        ?>
        <header id="myCarousel" class="carousel slide">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                    <?php
                    foreach ($sliderModel as $item):
                    ?>
                    <li data-target="#myCarousel" data-slide-to="<?= $item->id;?>" ></li>
                    <?php endforeach; ?>

            </ol>

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <?php
                $i=0;
                foreach ($sliderModel as $item):
                ?>
                <div class="item <?= ($i == 0)?  'active' :  '';?>">
                    <img src="<?= Yii::getAlias('@storage-url').'/image/front/slider/'.$item->file_name?>" alt="Los Angeles">
                </div>
                <?php
                    $i++;
                      endforeach;
                ?>



            </div>


            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-next"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-prev"></span>
            </a>

        </header>
    </div>
</div>
    <div class="container" style="margin-top:100px;">

        <?= $content ?>
    </div>
</div>

<div class="footer">
    <div class="container">

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
