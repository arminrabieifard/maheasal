<?php

namespace app\controllers;

use app\models\TellUs;
use Yii;
use yii\web\Controller;
use app\models\AboutUs;

class AboutUsController extends Controller
{

    public function actionIndex()
    {
        $model = AboutUs::findOne(1);
        $tellmodel = TellUs::findOne(1);

        return $this->render('index', [
            'model' => $model,
            'tellmodel' => $tellmodel,
        ]);
    }
}