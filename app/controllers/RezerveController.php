<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/20/2018
 * Time: 9:05 PM
 */

namespace app\controllers;


use app\models\Rezerve;
use Yii;
use yii\web\Controller;

class RezerveController extends Controller
{

    public function actionIndex()
    {
        $model = new Rezerve();
        if ($model->load(Yii::$app->request->post())) {
            $model->create_at = time();

            $model->ip = $_SERVER['REMOTE_ADDR'];
            if($model->save()){
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}