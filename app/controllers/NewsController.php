<?php
/**
 * Created by PhpStorm.
 * User: lvl
 * Date: 11/17/2017
 * Time: 8:18 AM
 */

namespace app\controllers;

use app\models\News;
use app\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['news'],
                        'allow' => false,
                        'roles' => ['@'],

                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {

        $model = News::find()->where(['visible' => 1]);

        $pages = new Pagination([
            'totalCount' => $model->count(),
            'defaultPageSize' => 20,
        ]);

        $model = $model->offset($pages->offset)->limit($pages->limit)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', [
            'model' => $model,
            'pages' => $pages
        ]);
    }
    public function actionView($id)
    {
        $model = News::find()->where(['id' => $id])->one();

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}