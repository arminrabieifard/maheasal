<?php

namespace app\controllers;
use alexantr\elfinder\InputFileAction;
use alexantr\elfinder\TinyMCEAction;
use Yii;
use yii\web\Controller;


class ElFinderController extends Controller
{
    public function actions()
    {
        return [
            'connector' => [
                'class' => \alexantr\elfinder\ConnectorAction::className(),
                'options' => [
                    'roots' => [
                        [
                            'driver' => 'LocalFileSystem',
                            'path' => (!is_dir(Yii::getAlias('@webroot') .'/uploads/tinymce') ? mkdir(Yii::getAlias('@webroot') . '/uploads/tinymce', 0777, true) : Yii::getAlias('@webroot') . '/uploads/tinymce'),
                            'URL' => Yii::getAlias('@web') . '/uploads/tinymce/',
                            'mimeDetect' => 'internal',
                            'imgLib' => 'gd',
                            'accessControl' => function ($attr, $path) {
                                // hide files/folders which begins with dot
                                return (strpos(basename($path), '.') === 0) ? !($attr == 'read' || $attr == 'write') : null;
                            },
                        ],
                    ],
                ],
            ],
            'input' => [
                'class' => InputFileAction::className(),
                'connectorRoute' => 'connector',
            ],
//            'ckeditor' => [
//                'class' => CKEditorAction::className(),
//                'connectorRoute' => 'connector',
//            ],
            'tinymce' => [
                'class' => TinyMCEAction::className(),
                'connectorRoute' => 'connector',
            ],

        ];
    }
}
