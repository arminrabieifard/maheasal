<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/6/2018
 * Time: 6:15 PM
 */

namespace app\controllers;


use app\models\UserMessage;
use Yii;
use yii\web\Controller;

class CommentController extends Controller
{

    public function actionIndex()
    {
        $model = new UserMessage();
        if ($model->load(Yii::$app->request->post())) {
            $model->create_at = time();
            $model->ip = $_SERVER['REMOTE_ADDR'];
            if($model->save()){
                Yii::$app->session->setFlash('alert', ['success', 'عملیات مورد نظر با موفقیت انجام  شد.']);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}