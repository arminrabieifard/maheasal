<?php
/**
 * Created by PhpStorm.
 * User: armin
 * Date: 3/2/2018
 * Time: 7:12 PM
 */

namespace app\controllers;


use app\models\MenuCategory;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\jui\Menu;
use yii\web\Controller;

class MenuController extends Controller
{
    public function behaviors() {
        parent::behaviors();
        return [
            'access' => [
                'class' => AccessControl::className(),
                'user'  => Yii::$app->user,
                'rules' => [
                    [
                        'actions' => ['important'],
                        'allow' => true,
                        'roles' => ['USer'],

                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['important'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

        ];
    }
    public function actionIndex()
    {
        $category = MenuCategory::find()->where(['visible' => 1])->all();



        return $this->render('index', [
            'category' => $category,

        ]);


    }
    public function actionImportant()
    {
        $category = MenuCategory::find()->where(['visible' => 1])->all();



        return $this->render('important', [
            'category' => $category,

        ]);


    }
}