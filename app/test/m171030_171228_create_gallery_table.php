<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gallery`.
 */
class m171030_171228_create_gallery_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gallery', [
            'id'          => $this->primaryKey(11)->unsigned(),
            'category_id' => $this->integer()->unsigned()->comment('مربوط به دسته'),
            'title'       => $this->string(255)->comment('عنوان'),
            'description' => $this->text()->comment('توضیحات'),
            'file_name'   => $this->string(50)->comment('نام فایل'),
            'images_file' => $this->text()->comment('عکسهای ذخیره شده'),
            'num_view'    => $this->integer()->defaultValue(0)->comment('تعداد بازدید'),
            'visible'     => $this->boolean()->defaultValue(1)->comment('نمایش داده شود؟'),
            'create_at'   => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);

        $this->createIndex(
            'idx-gallery-category_id',
            'gallery',
            'category_id'
        );

        $this->addForeignKey(
            'fk-gallery-category_id',
            'gallery',
            'category_id',
            'category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-gallery-category_id',
            'gallery'
        );

        $this->dropIndex(
            'idx-gallery-category_id',
            'category'
        );

        $this->dropTable('gallery');
    }
}
