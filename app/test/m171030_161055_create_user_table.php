<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171030_161055_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id'                     => $this->primaryKey(11)->unsigned(),
            'full_name'              => $this->string(50)->comment('نام و نام خانوادگی'),
            'user_name'              => $this->string(30)->unique()->notNull()->comment('نام کاربری'),
            'password'               => $this->string(250)->notNull()->comment('رمز عبور'),
            'auth_key'               => $this->string(32)->null(),
            'password_reset_token'   => $this->string(50)->null(),
            'email'                  => $this->string(250)->comment('ایمیل'),
            'email_active'           => $this->boolean()->defaultValue(0)->comment('وضعیت ایمیل'),
            'email_activation_token' => $this->string(50)->null(),
            'active'                 => $this->boolean()->defaultValue(1)->comment('وضعیت'),
            'create_at'              => $this->integer()->notNull()->defaultValue(0)->comment('زمان افزودن'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
